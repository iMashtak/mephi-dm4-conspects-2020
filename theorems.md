@import "style.css"

# Теоремы

## Пропозициональное исчисление

### Теоремы с импликацией и отрицанием

Формулы обозначаются буквами из-за существования изоморфизма Карри-Говарда между логическими формулами и типами. Дополнительно: <a src="https://ru.wikipedia.org/wiki/Соответствие_Карри_—_Ховарда">про изоморфизм Карри-Говарда</a> и <a src="https://ru.wikibooks.org/wiki/Комбинаторы_—_это_просто!">про комбинаторы K, S, I</a>.

|Обозначение|Запись|
|---|:---:|
|$`K`$|$`p \supset (q \supset p)`$
|$`S`$|$`s \supset (p \supset q) \supset (s \supset p \supset (s \supset q))`$
|$`E\lnot`$ in $`P_1`$|$`p \supset f \supset f \supset p`$
|$`E\lnot`$ in $`P_2`$|$`\overline{\overline{p}} \supset p`$
|$`I`$|$`p \supset p`$
|$`I\lnot`$|$`p \supset \overline{\overline{p}}`$
|$`P2`$|$`f \supset p`$
|$`P3`$|$`\overline{p} \supset (p \supset q)`$
|$`B`$|$`q \supset s \supset (p \supset q \supset (p \supset s))`$
|$`B'`$|$`p \supset q \supset (q \supset s \supset(p \supset s))`$
|$`C`$|$`p \supset (q \supset s) \supset (q \supset (p \supset s))`$
|$`C\lnot`$|$`\overline{p} \supset \overline{q} \supset (q \supset p)`$
|$`C\lnot'`$|$`p \supset q \supset (\overline{q} \supset \overline{p})`$

### Определения

Записываются в общем случае как $`Def`$ + логический оператор.

|Обозначение|Запись|
|---|:---:|
|$`Def(t)`$|$`t := p \supset p`$|
|$`Def(f)`$|$`f := \overline{p \supset p}`$|
|$`Def\lor`$|$`p\lor q := p \supset q \supset q`$|
|$`Def\land`$|$`p\land q := \overline{\overline{p} \lor \overline{q}}`$|
|$`Def\equiv`$|$`(p \equiv q) := (p \supset q) \land (q \supset p)`$|

### Теоремы с эквивалентностью

Записываются как $`\equiv`$ + другие логические связки, о взаимосвязи которых говорится в теореме. Здесь и далее:
* $`Cmt`$ - свойство коммутативности операции
* Индекс у обозначения ранее введённой для импликации теоремы обозначает её аналог для рассматриваемой операции. Например, здесь $`B_{\equiv}`$ - это теорема $`B`$, сформулированная для эквивалентности.

|Обозначение|Запись|
|---|:---:|
|$`\equiv^{\lnot}`$|$`p \equiv \overline{\overline{p}}`$|
|$`\equiv^{\supset}_R`$|$`p \equiv q \vdash (p \supset r) \equiv (q \supset r)`$|
|$`\equiv^{\supset}_L`$|$`p \equiv q \vdash (r \supset p) \equiv (r \supset q)`$|
|$`B_{\equiv}`$|$`p \equiv q, q \equiv r \vdash p \equiv r`$|
|$`Cmt_{\equiv}`$|$`p \equiv q \vdash q \equiv p`$|

### Теоремы с дизъюнкцией и конъюнкцией

|Обозначение|Запись|
|---|:---:|
|$`And`$|$`p, q \vdash p \land q`$|
|$`\lor_L`$|$`p \supset (p \lor q)`$|
|$`\lor_R`$|$`q \supset (p \lor q)`$|
|$`\land_L`$|$`(p \land q) \supset p`$|
|$`\land_R`$|$`(p \land q) \supset q`$|
|$`Impl_{\land}^{\lor}`$|$`p\land q \vdash p \lor q`$|
|$`Cmt_{\lor}`$|$`p \lor q \vdash q \lor p`$|
|$`Cmt_{\land}`$|$`p \land q \vdash q \land p`$|
|$`C_{\lor}`$|$`p \lor (q \lor r) \vdash q \lor (p \lor r)`$|
|$`C_{\land}`$|$`p \land (q \land r) \vdash q \land (p \land r)`$|