@import "styles.css"

# БДЗ 1

> Б100-500
> Иванов И.И.

---

**Задание 1.** 

* $`A \land B \supset A`$

*Решение.*

```math
khe
```

* $`A \land B \supset B`$

*Решение.*

```math
khe
```

---

**Задание 2.** $`A \land B \supset A \lor B`$

*Решение.*

```math
khe
```

---

**Задание 3.** $`(A \supset B) \land (A \supset C) \supset (A \supset B \land C)`$

*Решение.*

```math
khe
```

---

**Задание 4.** $`(A \equiv B) \supset (\overline{A} \equiv \overline{B})`$

*Решение.*

```math
khe
```

---

**Задание 5.** $`\overline{\overline{\overline{A \lor B}}} \equiv \overline{\overline{\overline{\overline{A}}} \supset B}`$

*Решение.*

```math
khe
```

---