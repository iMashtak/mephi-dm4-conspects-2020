# Шаблоны набора формул

## Исчисления $`P_1`$ и $`P_2`$

Чтобы записать доказательство теоремы используйте сниппет `\proof` или следующий код:

```latex
\begin{array}{llr}

\end{array}
```

Шаг вывода удобно записывать при помощи сниппета `\itemproof`:

```latex
1.  & <формула>
    & <обоснование или применяемое правило вывода>
    \\
```

Шаг вывода необходимо размещать внутри "доказательства теоремы". Пример записи нескольких шагов вывода доказательства теоремы:

```latex
\begin{array}{llr}
    1.  & p \vdash q \supset p
        & K
        \\
    2.  & (p \supset r) \vdash q \supset (p \supset r)
        & [p \supset r/p]1
        \\
\end{array}
```

## Исчисление секвенций $`LK`$

Здесь всё несколько сложнее, так как вывод может распадаться на несколько ветвей.

Начинать доказательство нужно со сниппета `\seqproof`:

```latex
\begin{array}{c}
    
    \\
\end{array}
```

Затем в строке над символами `\\` примените сниппет `\proof`. Каждый шаг вывода, опять же, оформляйте через `\itemproof`.

Когда приходит необходимость создавать ветви вывода, после символов `\\` используйте сниппет `\divproof`:

```latex
\begin{array}{c|c}

    &

\end{array}
```

В строках до и после символа `&` используйте сниппет `\seqproof`, затем `\proof` и так далее.

Исходник доказательства закона Пирса из 8го семинара:

```latex
\begin{array}{c}
    \begin{array}{llr}
        1.  & \vdash ((p \supset q) \supset p) \supset p
            & \supset_R
            \\
        2.  & (p \supset q) \supset p \vdash p
            & \supset_L
            \\
    \end{array}
    \\
    \begin{array}{c|c}
        \begin{array}{c}
            \begin{array}{llr}
                3.  & \vdash p, p \supset q
                    & \supset_R
                    \\
                5.  & p \vdash p, q
                    & W_R
                    \\
                6.  & p \vdash p
                    & \checkmark
                    \\
            \end{array}
            \\
        \end{array}
        &
        \begin{array}{c}
            \begin{array}{llr}
                4.  & p \vdash p
                    & \checkmark
                    \\
            \end{array}
            \\
        \end{array}
    \end{array}
\end{array}
```