@import "style.css"

# Семинар 14

> 29.05.20

## Модальная логика и исчисления

**Минимальная логика $`K`$**

Аксиомы:
- Аксиомы пропозиционального исчисления
- ($`AK`$): $`\Box(A \supset B) \supset (\Box A \supset \Box B)`$ - аксиома Крипке, основная модальная аксиома

Правила вывода:
- $`MP`$: $`A, A \supset B \vdash B`$
- $`\Box`$: $`A \vdash \Box A`$
- Подстановка $`Sub`$: $`A \vdash [B/p]A`$

Такой минимальной логике соответствует любая произвольная шкала Крипке. Добавление каждого из свойств регулируется следующей аксиомой:
- ($`R\Box`$): $`\Box p \supset p`$ (рефлексивность)
- ($`T\Box`$): $`\Box p \supset \Box \Box p`$ (транизитивность)
- ($`S\Box`$): $`p \supset \Box \Diamond p`$ (симметричность)

Добавляя каждую такую аксиому к минимальному исчислению будем получать новое исчисление. Среди возможных вариантов выделяют следующие 3:
- Исчисление $`S3`$: $`AK + R\Box`$
- Исчисление $`S4`$: $`AK + R\Box + T\Box`$
- Исчисление $`S5`$: $`AK + R\Box + T\Box + S\Box`$

*Теоремы, доказанные в исчислении $`K`$ верны также и в исчислениях $`S3`$, $`S4`$, $`S5`$.*

Имеется следующая фраза:
> Аксиомы пропозиционального исчисления

Но не сказано - какого именно исчисления. Значит, можно выбирать подходящее.

### Доказательство теорем

**Теорема.** ($`I\Diamond`$): $`p \supset \Diamond p`$
```math
\begin{array}{llr}
    1.  & \vdash p \supset q \supset (\overline{q} \supset \overline{p})
& C\lnot'
\\
    2.  & \vdash \Box \overline{p} \supset \overline{p} \supset (\overline{\overline{p}} \supset \overline{\Box \overline{p}})
& [\Box\overline{p}/p, \overline{p}/q]1
\\
    3.  & \vdash \Box p \supset p
& R\Box
\\
    4.  & \vdash \Box \overline{p} \supset \overline{p}
& [\overline{p}/p]3
\\
    5.  & \vdash \overline{\overline{p}} \supset \overline{\Box\overline{p}}
& MP(4,2)
\\
    6.  & \vdash p \supset \overline{\Box\overline{p}}
& ES(\equiv^{\lnot}, 5)
\\
    7.  & \vdash p \supset \Diamond p
& Def_{\Diamond}(6)
\\
\end{array}
```

Выполнима только в исчислении $`S3`$, то есть только на рефлексивных шкалах.

**Теорема.** ($`I_{\Diamond}^{\Box}`$): $`\Box p \supset \Diamond p`$
```math
\begin{array}{llr}
    1.  & \vdash \Box p \supset p
        & R\Box
        \\
    2.  & \vdash p \supset \Diamond p
        & I\Diamond
        \\
    3.  & \vdash \Box p \supset \Diamond p
        & TD(MP(1), 2)
        \\
\end{array}
```
Опять же, работает только на рефлексивных шкалах.
> Такая ситуация как раз и получилась на предыдущем занятии

### Строгие логические связки

В модальной логике, наряду с материальными связками $`\supset`$ и $`\equiv`$, вводятся также строгие связки $`\Rightarrow`$ и $`\Leftrightarrow`$, которые определяются следующим образом:
- $`p \Rightarrow q := \Box(p \supset q)`$
- $`p \Leftrightarrow q := \Box(p \equiv q)`$

**Теорема.** ($`K_{\Box}`$): $`p \Rightarrow (q \Rightarrow p)`$
```math
\begin{array}{llr}
    1.  & p \vdash q \supset p
        & MP(K)
        \\
    2.  & q \supset p \vdash \Box(q \supset p)
        & [q \supset p/p]\Box
        \\
    3.  & p \vdash \Box(q \supset p)
        & MP(2, TD(3))
        \\
    4.  & \vdash p \supset \Box(q \supset p)
        & TD(4)
        \\
    5.  & \vdash \Box(p \supset \Box(q \supset p))
        & \Box(5)
        \\
    6.  & \vdash p \Rightarrow (q \Rightarrow p)
        & Def_{\Rightarrow}
        \\
\end{array}
```

> По идее, справедлив "строгий" MP и теорема дедукции для строгих связок.

* $`p \Rightarrow (p \Rightarrow q \Rightarrow q)`$

### Другие теоремы

* $`\Box p = \overline{\Diamond \overline{p}}`$
* $`\Diamond p = \overline{\Box \overline{p}}`$

**Теорема.** $`\Box p \supset p \equiv p \supset \Diamond p`$
Необходимость: $`\Box p \supset p \supset (p \supset \Diamond p)`$
```math
    \begin{array}{llr}
        1.  & \vdash p \supset q \supset (\overline{q} \supset \overline{p})
            & C\lnot'
            \\
        2.  & \vdash \Box \overline{p} \supset \overline{p} \supset (\overline{\overline{p}} \supset \overline{\Box \overline{p}})
            & [\Box \overline{p}/p,\overline{p}/q]1
            \\
        % 3.  & \vdash p \supset \overline{\Diamond p} \supset (\overline{\Diamond p} \supset \overline{p})
        %    & [\overline{\Diamond p}/q]1
        %    \\
        3.  & \Box p \supset p \vdash \Box \overline{p} \supset \overline{p}
            & Sub(\overline{p}/p, R\Box)
            \\
        4.  & \vdash \Box p \supset p \supset (p \supset \Diamond p)
            & TD(MP(3,2))
            \\
    \end{array}
```
