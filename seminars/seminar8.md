@import "style.css"

# Семинар 8

> 10.04.20

## Исчисление секвенций $`LK`$

**Определение.** *Схема формулы* - формула, записанная с точностью до некоторой подстановки.

$`\vdash A \supset (B \supset A)`$

* $`p \supset (q \supset p)`$
* $`q \supset (p \supset q)`$
* $`(p \supset r) \supset (s \supset (p \supset r))`$

**Определение.** *Секвенция* - формула вида $`\Gamma \vdash \Delta`$, где $`\Gamma`$ и $`\Delta`$ - списки формул.

* $`A, B \supset C \vdash A \lor B, C`$

**Формулировка исчисления $`LK`$**

1. Аксиома $`A \vdash A`$
2. Правила вывода (см. мануал раздел 2.12)

**Определение.** *Выводом* формулы $`A`$ в исчислении секвенций будем считать вывод секвенции $`\vdash A`$.

*Как интерпретировать формулу исчисления секвенций, что такое "списки формул"?*

$`\land_L`$, $`\lor_R`$

* $`A, B \vdash C, D`$ эквивалентно $`A \land B \vdash C \lor D`$

*Как сопоставляются пропозициональные исчисления $`P_1`$, $`P_2`$ и секвенций $`LK`$?*

**Пример.** Доказать закон Пирса $`p \supset q \supset p \supset p`$

```math
\begin{array}{c}
    \begin{array}{llr}
        1.  & \vdash ((p \supset q) \supset p) \supset p
            & \supset_R
            \\
        2.  & (p \supset q) \supset p \vdash p
            & \supset_L
            \\
    \end{array}
    \\
    \begin{array}{c|c}
        \begin{array}{c}
            \begin{array}{llr}
                3.  & \vdash p, p \supset q
                    & \supset_R
                    \\
                5.  & p \vdash p, q
                    & W_R
                    \\
                6.  & p \vdash p
                    & \checkmark
                    \\
            \end{array}
            \\
        \end{array}
        &
        \begin{array}{c}
            \begin{array}{llr}
                4.  & p \vdash p
                    & \checkmark
                    \\
            \end{array}
            \\
        \end{array}
    \end{array}
\end{array}
```

**Задача 2.8.** $`p \lor \overline{p}`$

```math
\begin{array}{c}
    \begin{array}{llr}
        1.  & \vdash p \lor \overline{p}
            & \lor_R
            \\
        2.  & \vdash p, \overline{p}
            & \lnot_R
            \\
        3.  & p \vdash p
            & \checkmark
            \\
    \end{array}
    \\
\end{array}
```

**Задача 2.14.** $`p \equiv \overline{\overline{p}}`$

```math
\begin{array}{c}
    \begin{array}{llr}
        1.  & \vdash (p \supset \overline{\overline{p}}) \land (\overline{\overline{p}} \supset p)
            & \land_R
            \\
    \end{array}
    \\
    \begin{array}{c|c}
        \begin{array}{c}
            \begin{array}{llr}
                2.  & \vdash p \supset \overline{\overline{p}}
                    & \supset_R
                    \\
                3.  & p \vdash \overline{\overline{p}}
                    & \lnot_R
                    \\
                4.  & p, \overline{p} \vdash
                    & \lnot_L
                    \\
                5.  & p\vdash p
                    & \checkmark
                    \\
            \end{array}
            \\
        \end{array}
        &
        \begin{array}{c}
            \begin{array}{llr}
                6.  & \vdash \overline{\overline{p}} \supset p
                    & \supset_R
                    \\
                7.  & \overline{\overline{p}} \vdash p
                    & \lnot_L
                    \\
                8.  & \vdash \overline{p}, p
                    & \lnot_R
                    \\
                9.  & p \vdash p
                    & \checkmark
                    \\
            \end{array}
            \\
        \end{array}
    \end{array}
\end{array}
```

**Задача 2.9.** $`(p \supset (q \supset s)) \equiv (q \supset (p \supset s))`$

* $`(p \supset (q \supset s)) \supset (q \supset (p \supset s))`$
* $`(q \supset (p \supset s)) \supset (p \supset (q \supset s))`$

```math
\begin{array}{c}
    \begin{array}{llr}
        1.  & \vdash (p \supset (q \supset s)) \supset (q \supset (p \supset s))
            & \supset_R
            \\
        2.  & p \supset (q \supset s) \vdash q \supset (p \supset s)
            & \supset_R
            \\
        3.  & p \supset (q \supset s), q, p \vdash s
            & \supset_L
            \\
    \end{array}
    \\
    \begin{array}{c|c}
        \begin{array}{c}
            \begin{array}{llr}
                4.  & q,p \vdash p, s
                    & W_R
                    \\
                6.  & q,p \vdash p
                    & W_L
                    \\
                7.  & p \vdash p
                    & \checkmark
                    \\
            \end{array}
            \\
        \end{array}
        &
        \begin{array}{c}
            \begin{array}{llr}
                5.  & q, p, q \supset s \vdash s
                    & \supset_L
                    \\
            \end{array}
            \\
            \begin{array}{c|c}
                \begin{array}{c}
                    \begin{array}{llr}
                        8.  & q,p \vdash q,s
                            & W_R
                            \\
                        \dots
                    \end{array}
                    \\
                \end{array}
                &
                \begin{array}{c}
                    \begin{array}{llr}
                        9.  & q,p,s \vdash s
                            & 
                            \\
                        \dots
                    \end{array}
                    \\
                \end{array}
            \end{array}
        \end{array}
    \end{array}
\end{array}
```

**Задача 2.15.** $`p \supset (p \supset \overline{q \lor p})`$ (таблица истинности и обратный вывод)

```math
\begin{array}{c}
    \begin{array}{llr}
        1.  & \vdash p \supset (p \supset \overline{q \lor p})
            & \supset_R
            \\
        2.  & p \vdash p \supset\overline{q \lor p}
            & \supset_R
            \\
        3.  & p, p \vdash \overline{q \lor p}
            & \lnot_R
            \\
        4.  & p, p, q \lor p \vdash
            & \lor_L
            \\
    \end{array}
    \\
    \begin{array}{c|c}
        \begin{array}{c}
            \begin{array}{llr}
                5.  & p, p, q \vdash
                    & \times
                    \\
            \end{array}
            \\
        \end{array}
        &
        \begin{array}{c}
            \begin{array}{llr}
                6.  & p, p, p \vdash
                    & \times
                    \\
            \end{array}
            \\
        \end{array}
    \end{array}
\end{array}
```