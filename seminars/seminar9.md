@import "style.css"

# Семинар 9

> 17.04.20

## Функциональное исчисление $`F_1`$

В формуле $`\forall x P(x,y)`$ переменная $`x`$ связанная, а $`y`$ свободная.

### Исходный базис

*Аксиомы*:
* ($`K`$) $`A \supset (B \supset A)`$
* ($`S`$) $`A \supset (B \supset C) \supset (A \supset B \supset (A \supset C))`$
* ($`C\lnot`$) $`\overline{A} \supset \overline{B} \supset (B \supset A)`$
* (аксиома обобщения консеквента) $`\forall x (A \supset B) \supset (A \supset \forall x B)`$, где $`x \notin FV(A)`$
* (аксиома уточнения) $`\forall x A \supset [a/x]A`$, где $`a \notin BV(A)`$

Пояснение к аксиоме обобщения консеквента:
* формула $`\forall x (p \supset P(x)) \supset (p \supset \forall x P(x))`$ является вариантом аксиомы обобщения консеквента
* формула $`\forall x (P(x) \supset Q(x)) \supset (P(x) \supset \forall x Q(x))`$ **не является** вариантом аксиомы обобщения консеквента

Пояснение к аксиоме уточнения:
* формулы $`\forall x P(x) \supset P(y)`$ и $`\forall x P(x) \supset P(x)`$ явяляются вариантами аксиомы
* формула $`\forall x \forall y P(x,y) \supset \forall y P(y,y)`$ **не является** вариантом аксиомы
* формула $`\forall x (P(x) \supset \forall y Q(x,y)) \supset (P(y) \supset \forall y Q(y,y))`$ **не является** вариантом аксиомы

*Правила вывода*:
* Modus ponens: $`A, A\supset B \vdash B`$
* Правило обобщения: $`A \vdash \forall x A`$

*Связь с пропозициональным исчислением через правило обобщения.*

```math
\exists x A := \overline{\forall x \overline{A}}
```

### Подстановки

Алфавитная замена:
* $`\widetilde{[y/x]}z = z`$
* $`\widetilde{[y/x]}P(z_1, z_2, ..., z_n)=P(z_1, z_2, ...,z_n)`$
* $`\widetilde{[y/x]}\overline{A} = \overline{A}`$
* $`\widetilde{[y/x]}(A \supset B)=A \supset B`$
* $`\widetilde{[y/x]}\forall x A = \forall y [y/x]A`$, если $`y \notin A`$
* $`\widetilde{[y/x]}\forall y A = \forall y A`$, если $`y \ne x`$ и $`y \notin A`$

Подстановка вместо индивидной переменной:
* $`[a/x]x=a`$
* $`[a/x]y=y`$, если $`x\ne y`$
* $`[a/x]P(y_1,...,y_n)=P([a/x]y_1,...,[a/x]y_n)`$
* $`[a/x]\overline{A}=\overline{[a/x]A}`$
* $`[a/x]A\supset B=[a/x]A \supset [a/x]B`$
* $`[a/x]\forall x A=\forall x A`$
* $`[a/x]\forall y A=\forall y ([a/x]A)`$, если $`x\ne y \land a\ne y`$
* $`[a/x]\forall y A=\forall y' ([a/x]([y'/y]A))`$, если $`a=y`$, причём $`y'\notin FV(A)`$ и $`y'\ne x`$
    Пояснение: $`[x/y]\forall x P(x,y) = [x/y] \forall x' P(x', y) = \forall x' P(x', x)`$

**Задача 1.** $`[y/x,a/y](\forall x P(x,y) \supset Q(x, x, y) \land R(y))`$

1. $`[y/x,a/y](\forall x P(x,y) \supset (Q(x, x, y) \land R(y)))`$
2. $`[y/x,a/y]\forall x P(x,y) \supset [y/x,a/y](Q(x, x, y) \land R(y))`$
3. $`\forall x P(x, a) \supset ([y/x,a/y]Q(x,x,y) \land [y/x,a/y]R(y))`$
4. $`\forall x P(x,a) \supset (Q(y,y,a) \land R(a))`$

**Задача 2.** $`[z/x, x/y, y/z](\forall x P(x) \supset \forall y \forall z Q(x,y,z))`$

1. $`[z/x, x/y, y/z]\forall x P(x) \supset [z/x, x/y, y/z]\forall y \forall z Q(x,y,z)`$
2. $`\forall x P(x) \supset \forall y \forall z' Q(z, y, z')`$

Подстановка вместо функциональной переменной:
* $`[C/P(x_1,\dots,x_n)]P(a_1,\dots,a_n)=[a_1/x_1,\dots,a_n/x_n]C`$
* $`[C/P(x_1,\dots,x_n)]Q(a_1,\dots,a_n)=Q(a_1,\dots,a_n)`$, если $`P\ne Q`$
* $`[C/P(x_1,\dots,x_n)](A \supset B)=[C/P(x_1,\dots,x_n)]A \supset [C/P(x_1,\dots,x_n)]B`$
* $`[C/P(x_1,\dots,x_n)]\overline{A} = \overline{[C/P(x_1,\dots,x_n)]A}`$
* $`[C/P(x_1,\dots,x_n)]\forall x A = \forall x [C/P(x_1,\dots,x_n)]A`$, если $`x \notin FV(C)`$
* $`[C/P(x_1,\dots,x_n)]\forall y A = \forall y' [C/P(x_1,\dots,x_n)] [y'/y]A`$, если $`y \in FV(C)`$, причём $`y' \notin FV(C)`$

**Задача 3.** $`[P(x) \land R(y)/Q(x,y)](Q(x,y) \supset Q(y,x) \supset R(x))`$

1. $`[P(x) \land R(y)/Q(x,y)](Q(x,y) \supset Q(y,x) \supset R(x))`$
2. $`[P(x) \land R(y)/Q(x,y)]Q(x,y) \supset [P(x) \land R(y)/Q(x,y)]Q(y,x) \supset [P(x) \land R(y)/Q(x,y)]R(x)`$
3. $`(P(x) \land R(y)) \supset (P(y) \land R(x)) \supset R(x)`$

* $`[P(u) \lor R(v,u)/Q(u,v)]Q(x,y) = P(x) \lor R(y,x)`$

**Задача 4.** $`[\forall y P(x,y)/P(x)](\forall x P(x) \supset P(y))`$

1. $`[\forall y P(x,y)/P(x)]\forall x P(x) \supset [\forall y P(x,y)/P(x)]P(y)`$
2. $`[\forall y P(x,y)/P(x)]\forall x' P(x') \supset [\forall y P(x,y)/P(x)]P(y)`$
3. $`\forall x' ([\forall y P(x,y)/P(x)]P(x')) \supset [\forall y P(x,y)/P(x)]P(y)`$
4. $`\forall x' \forall y P(x', y) \supset [\forall y P(x,y)/P(x)]P(y)`$
5. $`\forall x' \forall y P(x', y) \supset \forall y' P(y, y')`$

### Теоремы исчисления $`F_1`$

**Задача 5.** $`[a/x]A \supset \exists x A`$, где $`x`$ --- индивидная переменная, а $`a`$ --- индивидная переменная или константа и $`a`$ не является связанной переменной ни в одной части формулы $`A`$.

```math
\exists x A := \overline{\forall x \overline{A}}
```

```math
[a/x] A \supset \overline{\forall x \overline{A}}
```

```math
\begin{array}{llr}
    1.  & \vdash \forall x \overline{A} \supset \overline{[a/x]A}
        & \text{аксиома уточнения}
        \\
    2.  & \vdash A \supset B \supset (\overline{B} \supset \overline{A})
        & C\lnot'
        \\
    3.  & (\forall x \overline{A} \supset \overline{[a/x]A}) \supset (\overline{\overline{[a/x]A}} \supset \overline{\forall x \overline{A}})
        & [\forall x \overline{A}/A, \overline{[a/x]A}/B]2
        \\
    4.  & \overline{\overline{[a/x]A}} \supset \overline{\forall x \overline{A}}
        & MP(1,3)
        \\
    5.  & [a/x]A \supset \overline{\forall x \overline{A}}
        & ES(\equiv^{\lnot}, 4)
        \\
\end{array}
```

### Домашнее задание

БДЗ 2: задания 1, 2, 3, 4.