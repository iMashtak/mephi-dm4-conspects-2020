@import "style.css"

# Семинар 12

> 15.05.20

## Интуиционизм

Либо предоставьте объект, либо дайте процедуру его получения.

### Исчисление $`P^i`$

Аксиомы:

* ($`K`$): $`A \supset (B \supset A)`$
* ($`S`$): $`A \supset (B \supset C) \supset (A \supset B \supset (A \supset C))`$
* ($`\land_L`$): $`A \land B \supset A`$
* ($`\land_R`$): $`A \land B \supset B`$
* ($`\lor_L`$): $`A \supset A \lor B`$
* ($`\lor_R`$): $`B \supset A \lor B`$
* ($`\supset_{\land}`$): $`A \supset (B \supset A \land B)`$
* ($`T\lor`$): $`(A \supset B) \supset ((B\supset C) \supset (A \lor B \supset C))`$
* ($`RdAb`$): $`(A \supset B) \supset ((A \supset \overline{B}) \supset \overline{A})`$
* ($`K\lnot`$): $`A \supset (\overline{A} \supset B)`$

Правила вывода: modus ponens, подстановка

Принцип сведения к абсурду:
```math
\frac{
    \vdash A \supset B \;\;\; \vdash A \supset \overline{B}
}{
    \vdash \overline{A}
}
```

Теорема дедукции работает, так как есть аксиомы $`K`$ и $`S`$.

### Модель исчисления $`P^i`$

Логическая матрица: $`\mathbf{M}=\langle M, 1, \cdot, +, \to, \sim \rangle`$

Переход из формул исчисления в модель осуществляется с помощью оценки $`f`$:
```math
\begin{array}{c}
f(A\land B)=f(A)\cdot f(B) \\
f(A \lor B)= f(A)+f(B) \\
f(A\supset B)=f(A) \to f(B) \\
f(\lnot A)=\sim f(A)
\end{array}
```

Формула исчисления *истинна* в модели $`\mathbf{M}`$, если $`f(A)=1`$.

**Теорема.** Пусть пропозициональное исчисление таково, что его единственным правилом вывода является $`\text{modus ponens}`$. Тогда логическая матрица $`\mathbf{M}`$ является моделью этого исчисления, если и только если все его аксиомы истинны в $`\mathbf{M}`$.

В качестве модели будем использовать *трёхзначную псевдобулеву алгебру*:
```math
\mathbf{M} = 
        \langle
            \{0, \frac{1}{2}, 1\}, \cdot, +, \to, \sim
        \rangle
```

```math
\begin{array}{ll}
            x \cdot y = \min(x,y) 
            &
            x + y = \max(x,y) 
            \\
            x \to y = \begin{cases}
                1 &\text{if}\ x \le y \\
                y &\text{else}
            \end{cases} 
            &
            \sim x = x \to 0 = \begin{cases}
                1 &\text{if}\ x = 0 \\ 
                0 &\text{else}
            \end{cases}
        \end{array}
```

**Задание.** Запишем таблицы истинности трёхзначной логики.
```math
\begin{array}{|c|c|c|c|}\hline
    A            & B & A \to B   \\\hline
    1            & 1      & 1            \\\hline
    1            & \frac{1}{2}      & \frac{1}{2} \\\hline
    1            & 0      & 0            \\\hline
    \frac{1}{2}  & 1      & 1 \\\hline
    \frac{1}{2}  & \frac{1}{2}      & 1 \\\hline
    \frac{1}{2}  & 0      & 0 \\\hline
    0            & 1      & 1            \\\hline
    0            & \frac{1}{2}      & 1            \\\hline
    0            & 0      & 1            \\\hline
\end{array}
```

```math
\begin{array}{|c|c|c|c|}\hline
    A + B & 0 & \frac{1}{2} & 1 \\\hline
    0 & 0 & \frac{1}{2} & 1 \\\hline 
    \frac{1}{2} & \frac{1}{2} & \frac{1}{2} & 1 \\\hline 
    1 & 1 & 1 & 1 \\\hline 
\end{array}
```

```math
\begin{array}{|c|c|c|c|}\hline
    A \cdot B & 0 & \frac{1}{2} & 1 \\\hline
    0 & 0 & 0 & 0 \\\hline 
    \frac{1}{2} & 0 & \frac{1}{2} & \frac{1}{2} \\\hline 
    1 & 0 & \frac{1}{2} & 1 \\\hline 
\end{array}
```

$`A \equiv B := (A \to B) \cdot (B \to A)`$

### Какие формулы исчисления высказываний не выводятся в интуиционистском?

**Пример.** $`A \lor \overline{A}`$ - закон исключённого третьего
```math
\begin{array}{|c|c|c|c|}\hline
    A            & \sim A & A + \sim A   \\\hline
    1            & 0      & 1            \\\hline
    \frac{1}{2} & 0      & \frac{1}{2} \\\hline
    0            & 1      & 1            \\\hline
\end{array}
```

**Пример.**  $`\overline{A} \supset \overline{B} \supset (B \supset A)`$ - аксиома $`C\lnot`$ исчисления $`P_2`$

**Пример.** $`\overline{\overline{A}} \supset A`$ - теорема $`E\lnot`$ исчисления $`P_2`$

### Какие формулы исчисления высказываний остались в интуиционистском?

Точно те, для вывода которых использовались только аксиомы $`P^i`$.

Например, теорема $`B`$ доказывается с помощью $`K`$, $`S`$. Значит, она точно так же доказывается в $`P^i`$.

**Теорема.** ($`I\lnot`$): $`A \supset \overline{\overline{A}}`$
* *Доказательство.*  
    Первым делом, проверим истинность:
    ```math
    \begin{array}{|c|c|c|c|c|}\hline
        A & \sim A & \sim \sim A & A \to \sim\sim A \\\hline
        1            & 0      & 1 & 1 \\\hline
        \frac{1}{2}  & 0      & 1 & 1 \\\hline
        0            & 1      & 0 & 1 \\\hline
    \end{array}
    ```
    Что ж, доказательство существует. У нас пока всего 2 аксиомы, работающие с отрицаниями: $`RdAb`$ и $`K\lnot`$. Их поиспользуем:
    ```math
    \begin{array}{llr}
        1.  & \vdash A \supset B \supset (A \supset \overline{B} \supset \overline{A})
            & RdAb
            \\
        2.  & \overline{A} \supset B \vdash \overline{A} \supset \overline{B} \supset \overline{\overline{A}} 
            & [\overline{A}/A]MP(1)
            \\
        3.  & \vdash A \supset (\overline{A} \supset B)
            & K\lnot
            \\
        4.  & A \vdash \overline{A} \supset B
            & MP(3)
            \\
        5.  & A \vdash \overline{A} \supset \overline{B}
            & [\overline{B}/B]4
            \\
        6.  & A \vdash \overline{A} \supset B \supset \overline{\overline{A}}
            & TD(MP(5,2))
            \\
        7.  & A, A \vdash \overline{\overline{A}}
            & MP(4,6)
            \\
        8.  & \vdash A \supset \overline{\overline{A}}
            & TD(7)
            \\
    \end{array}
    ```

### Связь КИВ и ИИВ

**Теорема** *о связи классического и интуиционистского*. Формула $`A`$ классического исчисления выводима тогда и только тогда, когда формула $`A^*`$ выводима в интуиционистском исчислении (высказываний/предикатов).
* $`A`$ - атом, значит $`A^*=\overline{\overline{A}}`$
* $`A=B \lambda C`$ - значит $`A^*=\overline{\overline{B^* \lambda C^*}}`$ ($`\lambda \in \{\supset, \lor, \land\}`$)
* $`A=\overline{B}`$ - значит $`A^*=\overline{B^*}`$
* $`A=\kappa xB`$ - значит $`A^*=\overline{\overline{\kappa x B^*}}`$ ($`\kappa=\{\forall, \exists\}`$)

**Теорема.** $`\overline{\overline{\overline{\overline{A}} \supset \overline{\overline{B}}}} \supset \overline{\overline{\overline{A} \supset \overline{\overline{B}} \supset \overline{\overline{B}}}}`$
* *Доказательство.* Теорема нам говорит "тогда и только тогда". Обозначим формулу, которую нам надо доказать как $`F^*`$, она принадлежит исчислению $`P^i`$. Тогда $`F`$ принадлежит исчислению $`P_2`$.
    Опишем переход к $`F`$:
    ```math
    \begin{array}{llr}
        1.  & \overline{\overline{\overline{\overline{A}} \supset \overline{\overline{B}}}} \supset \overline{\overline{\overline{A} \supset \overline{\overline{B}} \supset \overline{\overline{B}}}}
            & 
            \\
        2.  & \overline{\overline{A}} \supset \overline{\overline{B}} \supset (\overline{A} \supset \overline{\overline{B}} \supset \overline{\overline{B}})
            & 
            \\
        3.  & A \supset B \supset (\overline{A} \supset B \supset B)
            & 
            \\
    \end{array}
    ```
    Получили, что $`F`$ - это $`A \supset B \supset (\overline{A} \supset B \supset B)`$. Теперь осталось привести доказательство этой теоремы в исчислении $`P_2`$.