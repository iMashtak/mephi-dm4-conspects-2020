@import "style.css"

# Семинар 11

> 8.05.20

## Краткий разбор КР 2

1. Modus ponens
    ```math
    \vdash (p \supset q) \supset (\overline{q} \supset \overline{p}) \\
    p \vdash q \supset (\overline{q} \supset \overline{p}) \text{ не надо} \\
    p \supset q \vdash \overline{q} \supset \overline{p}
    ```
2. And
    ```math
    p,q \vdash p \land q
    ```
    ```math
    p \supset (q \supset r) \equiv p \land q \supset r
    ```

## Исчисление секвенций для предикатов

Аксиомы и правила вывода: те же, что и для пропозиционального исчисления. Добавляются 4 правила (о кванторах):

* ($`\forall_L`$) $`\cfrac{\Gamma,  \forall x F(x), F(t) \vdash \Delta}{\Gamma, \forall x F(x) \vdash \Delta}`$
* ($`\forall_R`$) $`\cfrac{\Gamma \vdash F(y),  \Delta}{\Gamma \vdash \forall x F(x), \Delta}`$
* ($`\exists_L`$) $`\cfrac{\Gamma,  F(y) \vdash \Delta}{\Gamma, \exists x F(x) \vdash \Delta}`$
* ($`\exists_R`$) $`\cfrac{\Gamma \vdash F(t), \exists x F(x), \Delta}{\Gamma \vdash \exists x F(x), \Delta}`$

***Важно:*** $`t`$ --- произвольная переменная, а $`y`$ --- переменная, не содержащаяся в формулах из $`\Gamma`$, $`\Delta`$.

Обратный вывод при помощи секвенций также называют *методом семантических таблиц Бета*.

**Пример 1.** Разберём, зачем нужны ограничения на переменные:

``` math
\begin{array}{c}
    \begin{array}{llr}

        1.  & \vdash \exists x P(x) \supset \forall x P(x)

    & \supset_R
    \\

        2.  & \exists x P(x) \vdash \forall x P(x)

    & \exists_L
    \\

        3.  & P(c_1) \vdash \forall x P(x)

    & \forall_R 
    \\

        4.  & P(c_1) \vdash P(c_2)

    & \text{FAILED}
    \\
    \end{array}
    \\
\end{array}
```

**Пример 2.** Теперь можно при выводе попадать в бесконечный цикл:

``` math
\begin{array}{c}
    \begin{array}{llr}

        1.  & \vdash \forall x \exists y P(x,y) \supset \exists y \forall x P(x,y)

    & \supset_R
    \\

        2.  & \underline{\forall x} \exists y P(x,y) \vdash \exists y \forall x P(x,y)

    & \forall_L
    \\

        3.  & \forall x \exists y P(x,y), \exists y P(c_1,y)\vdash \underline{\exists y} \forall x P(x,y)

    & \exists_R
    \\

        4.  & \forall x \exists y P(x,y), \underline{\exists y} P(c_1,y) \vdash \exists y \forall x P(x,y), \forall x P(x,c_2)

    & \exists_L
    \\

        5.  & \forall x \exists y P(x,y), \exists y P(c_1,c_3)\vdash \exists y \forall x P(x,y), \underline{\forall x} P(x,c_2)

    & \forall_R
    \\

        6.  & \forall x \exists y P(x,y), P(c_1,c_3) \vdash \exists y \forall x P(x,y),  P(c_4,c_2)

    & \infty
    \\
    \end{array}
    \\
\end{array}
```

**Пример 3.**

``` math
\begin{array}{c}
    \begin{array}{llr}

        1.  & \vdash \forall x F(x) \supset \exists x F(x)

    & \supset_R 
    \\

        2.  &  \forall x F(x) \vdash \exists x F(x)

    & \forall_L
    \\

        3.  & \forall x F(x), F(t) \vdash \exists x F(x)

    & \exists_R
    \\

        4.  & \forall x F(x), F(t) \vdash \exists x F(x), F(t)

    & W_R
    \\

        5.  & \forall x A, F(t) \vdash F(t)

    & W_L
    \\

        6.  & F(t) \vdash F(t)

    & \checkmark
    \\
    \end{array}
\end{array}
```

## Сколемизация

Формула находится в *предварённой нормальной форме*, если она имеет вид $`Q_1x_1\dots Q_nx_n(A)`$, где

* $`Q_1,...,Q_n \in \{\forall, \exists\}`$
* $`A`$ --- бескванторная формула в конъюктивной нормальной форме (КНФ)

- $`\forall x \exists y (F(x) \land (G(y,x) \lor F(y)))`$
- КНФ: $`A_1 \land A_2 \land ... \land A_n`$. Но $`A_i = B_1 \lor B_2 \lor ... \lor B_m`$. $`B_i = p`$ или $`B_i = \overline{p}`$

**Алгоритм приведения к предварённой нормальной форме**

* Переименовываем переменные под кванторами так, чтобы все имена переменных были различными.
* Если в формуле есть свободные переменные, то добавляем в начало формулы кванторы всеобщности для этих переменных.
* Приводим формулу к КНФ (конъюктивной нормальной форме) при помощи следующих эквивалентностей (учитывая коммутативность конъюнкции и дизъюнкции):
    - $`(A \equiv B) \equiv ((A \supset B) \land (B \supset A))`$
    - $`(A \supset B) \equiv (\overline{A} \lor B)`$
    - $`\overline{A \land B} \equiv (\overline{A} \lor \overline{B})`$
    - $`\overline{A \lor B} \equiv (\overline{A} \land \overline{B})`$
    - $`\overline{\overline{A}} \equiv A`$
    - $`A \lor (B \land C) \equiv (A \lor B) \land (A \lor C)`$
    - $`\overline{\forall x F(x)} \equiv \exists x \overline{F(x)}`$
    - $`\overline{\exists x F(x)} \equiv \forall x \overline{F(x)}`$

* Выносим кванторы из-под скобок по следующим законам:
    - $`(\forall x F(x) \land A) \equiv \forall x (F(x) \land A)`$
    - $`\forall x (F(x) \land \forall y G(y)) \equiv \forall x \forall y (F(x) \land G(y))`$

Записаны формулы только для операции $`\land`$ и квантора $`\forall`$, однако справедливы и для операции $`\lor`$ и квантора $`\exists`$ в любых комбинациях. Также необходимо учитывать коммутативность конъюнкции и дизъюнкции.

**Определение.** Формула находится в *сколемовской стандартной форме*, если она находится в предварённой нормальной форме и не содержит кванторов существования.

**Лемма.** Пусть $`A=\forall x_1 \dots \forall x_n \exists x_{n+1}B`$ --- замкнутая формула ($`n\ge 0`$) и функциональная константа $`f`$ не содержится в $`B`$. Тогда $`A`$ выполнима ттт, когда выполнима формула $`\forall x_1\dots \forall x_n ([f(x_1, \dots, x_n)/x_{n+1}]B)`$. Если $`n=0`$, то лемма справедлива для $`f=c`$, т.е. $`f`$ --- функциональная константа без аргументов.

* $`\forall x \exists y F(x,y)`$. $`y \to f(x)`$ => $`\forall x F(x, f(x))`$.

**Пример сколемизации.**:

```math
\begin{array}{llr}
    1.  & P(x) \supset (\forall y G(y) \lor \exists y (R(x,y) \land \exists x P(x) \supset \exists z G(z)))
        & \text{rename and $\forall$}
        \\
    2.  & \forall x 
    (
        P(x) \supset 
        (
            \forall y' G(y') \lor \exists y 
            (
                R(x,y) \land \exists x' P(x') \supset \exists z G(z)
            )
        )
    )
        & \supset \text{remove}
        \\
    3.  & \forall x 
    (
        \overline{P(x)} \lor 
        (
            \forall y' G(y') \lor \exists y 
            (
                \overline{R(x,y) \land \exists x' P(x')} \lor \exists z G(z)
            )
        )
    )
        & \text{де Морган}
        \\
    4.  & \forall x 
    (
        \overline{P(x)} \lor 
        (
            \forall y' G(y') \lor \exists y 
            (
                \overline{R(x,y)} \lor \forall x' \overline{P(x')} \lor \exists z G(z)
            )
        )
    )
        & \text{сколемизируем}
        \\
    5.  & \forall x 
    (
        \overline{P(x)} \lor 
        (
            \forall y' G(y') \lor \exists y \forall x' \exists z
            (
                \overline{R(x,y)} \lor \overline{P(x')} \lor G(z)
            )
        )
    )
        & \text{сколемизируем}
        \\
    6.  & \forall x \forall y' \exists y \forall x' \exists z
    (
        \overline{P(x)} \lor 
        (
            G(y') \lor 
            (
                \overline{R(x,y)} \lor \overline{P(x')} \lor G(z)
            )
        )
    )
        & \text{лишние скобки}
        \\
    7.  & \forall x \forall y' \exists y \forall x' \exists z
    (
        \overline{P(x)} \lor G(y') \lor \overline{R(x,y)} \lor \overline{P(x')} \lor G(z)
    )
        & y \to f(x,y')
        \\
    8.  & \forall x \forall y' \forall x' \exists z
    (
        \overline{P(x)} \lor G(y') \lor \overline{R(x,f(x,y'))} \lor \overline{P(x')} \lor G(z)
    )
        & z \to g(x,y',x'')
        \\
    9.  & \forall x \forall y' \forall x'
    (
        \overline{P(x)} \lor G(y') \lor \overline{R(x,f(x,y'))} \lor \overline{P(x')} \lor G(g(x,y',x''))
    )
        & \checkmark
        \\
\end{array}
```

**Пример неоднозначной ситуации:**

Рассмотрим случай, когда у нас есть выбор: какой квантор переносить первым за скобки. Возьмём формулу, уже находящуюся в КНФ:
```math
\forall z \exists y P(y,z) \land \exists x G(x)
```

Здесь есть два варианта:
* $`\exists x \forall z \exists y (P(y,z) \land G(x))`$  
    В этом случае будут выбраны следующие функциональные константы: $`x \to c`$, $`y \to f(z)`$.
* $`\forall z \exists y \exists x (P(y,z) \land G(x))`$  
    В этом случае $`y \to f(z)`$, $`x \to g(z)`$.

$`x \to g(z)=c`$

## Общезначимость и выполнимость

**Определение.** Множество формул $`\Gamma`$ называется *противоречивым*, если существует такая формула $`B`$, что $`\Gamma \vdash B`$ и $`\Gamma \vdash \overline{B}`$. Если же такой формулы не существует, то множество $`\Gamma`$ называется *непротиворечивым*.

**Определение.** Множество формул $`\Gamma`$ называется *совместным*, если все формулы из $`\Gamma`$ могут быть одновременно истинными в некоторой модели.

**Теорема.** Множество $`\Gamma`$ непротиворечиво тогда и только тогда, когда оно совместно.

Таким образом, одним из способов доказательства непротиворечивости некоторого множества формул является построение модели для этого множества.

Формула выполнима, если существует хотя бы одна модель, в которой она истинна.

Формула общезначима, если она выполнима в любой модели.

$`\forall x P(x)`$