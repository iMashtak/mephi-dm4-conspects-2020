@import "style.css"

# Семинар 7

> 27.03.20

## Применение производного правила вывода и подстановочности эквивалентности

На прошлом семинаре было доказано производное правило вывода:
```math
\frac {
    \vdash A \;\;\; \vdash B
}{
    \vdash A \land B
}
\tag{And}
```

В то же время определение операции эквивалентности выглядит так:
```math
A \equiv B := (A \supset B) \land (B \supset A)
```

В основном правило вывода $`And`$ будет использоваться как раз для доказательства теорем с эквивалентностью. Сначала будет доказываться следование в одну сторону, затем следование в другую сторону. Покажем этот механизм на простейшем примере.

* **Теорема.** $`\equiv\lnot`$: $`p \equiv \overline{\overline{p}}`$
    ```math
    \begin{array}{llr}
        1.  & \vdash p \supset \overline{\overline{p}}
            & I\lnot
            \\
        2.  & \vdash \overline{\overline{p}} \supset p
            & E\lnot
            \\
        3.  & \vdash (p \supset \overline{\overline{p}}) \land (\overline{\overline{p}} \supset p)
            & And(1,2)
            \\
        4.  & \vdash p \equiv \overline{\overline{p}}
            & Def\equiv(3)
            \\
    \end{array}
    ```

## Решение задач

* **Задание 3.** (из ДЗ2-1) $`\overline{p} \supset q \supset (\overline{q} \supset p)`$
    Вариант 1 - без теоремы о подстановочности эквивалентности
    ```math
    \begin{array}{llr}
        1.  & \vdash \overline{p} \supset \overline{\overline{q}} \supset (\overline{q} \supset p)
            & [\overline{q}/q]C\lnot
            \\
        2.  & \overline{p} \vdash \overline{\overline{q}} \supset (\overline{q} \supset p)
            & MP(1)
            \\
        3.  & q \vdash \overline{\overline{q}}
            & MP(I\lnot)
            \\
        4.  & \overline{p}, q \vdash \overline{q} \supset p
            & MP(3,2)
            \\
        5.  & \vdash \overline{p} \supset q \supset (\overline{q} \supset p)
            & TD(4)
            \\
    \end{array}
    ```

    Вариант 2 - с подстановочностью эквивалентности
    ```math
    \begin{array}{llr}
        1.  & \vdash \overline{p} \overline{\overline{q}} \supset (\overline{q} \supset p)
            & [\overline{q}/q]C\lnot
            \\
        2.  & \vdash \overline{p} \supset q \supset (\overline{q} \supset p)
            & ES(\equiv\lnot, 1)
            \\
    \end{array}
    ```

**Замечание.** При выводе использовалась запись правила вывода modus ponens с элементами за знаком "штопора". Полная запись выглядит так:
```math
\frac {
    \Gamma \vdash A, \;\;\; \Delta \vdash A \supset B
}{
    \Gamma, \Delta \vdash B
}
```

Здесь $`\Gamma`$ и $`\Delta`$ - множество формул, перечисленных через запятую. Порядок следования этих формул нам не важен, вследствие теоремы дедукции.

**Замечание.** На деле, абсолютно все правила вывода можно переписать с такими "контекстами" за знаком "штопора". Например, только что введённое правило $`And`$:
```math
\frac {
    \Gamma \vdash A \;\;\; \Delta \vdash B
}{
    \Gamma, \Delta \vdash A \land B
}
```

* **Задание 4.** (из ДЗ2-1) $`\overline{\overline{\overline{p}}} \supset q \supset (\overline{\overline{\overline{q}}} \supset p)`$
    Доказывается путём двухкратного применения $`ES(\equiv\lnot, \text{Задание 3})`$.

* **Задание 5.** (из ДЗ2-1) $`\overline{p \lor \overline{q}} \supset \overline{p}`$
    По виду формулы, очевидно, что нужно попытаться использовать теорему $`C\lnot'`$, чтобы совершить в неё подстановку и в правой части получить искомое выражение.
    ```math
    \begin{array}{llr}
        1.  & \vdash p \supset (p \lor \overline{q}) \supset (\overline{p \lor \overline{q}} \supset \overline{p})
            & [p \lor \overline{q}/q]C\lnot'
            \\
        2.  & \vdash p \supset (p \lor \overline{q})
            & [\overline{q}/q] \lor_L
            \\
        3.  & \vdash \overline{p \lor \overline{q}} \supset \overline{p}
            & MP(2,1)
            \\
    \end{array}
    ```

* **Задание 11.** (упрощённая версия) $`p \equiv q \vdash (r \supset p) \equiv (r \supset q)`$.
    Доказательство эквивалентности двух утверждений обычно сводится к доказательству двух утверждений: следование направо и следование налево. В нашем случае это расписывается так:
    * (направо) $`p \equiv q \vdash (r \supset p) \supset (r \supset q)`$
    * (налево) $`p \equiv q \vdash (r \supset q) \supset (r \supset p)`$

    Если мы докажем оба эти утверждения, то по правилу вывода $`And`$ получим искомое утверждение.

    Докажем только одно из этих утверждений:
    ```math
    \begin{array}{llr}
        1.  & \vdash q \supset (r \supset q)
            & [q/p, r/q]K
            \\
        2.  & p \equiv q \vdash p \equiv q
            & [p \equiv q/p]I
            \\
        3.  & p \equiv q \vdash (p \supset q) \land (q \supset p)
            & Def\equiv(2)
            \\
        4.  & p \equiv q \vdash (p \supset q)
            & MP(3, \land_L)
            \\
        5.  & p \equiv q, p \vdash q
            & MP(4)
            \\
        6.  & p \equiv q, p \vdash r \supset q
            & MP(5, 1)
            \\
        7.  & p \equiv q, p, r \vdash q
            & MP(6)
            \\
        8.  & p \equiv q, r, p \vdash q
            & TD(7)
            \\
        9.  & p \equiv q \vdash r \supset (p \supset q)
            & TD(8)
            \\
        10. & \vdash r \supset (p \supset q) \supset (r \supset p \supset (r \supset q))
            & [r/s]S
            \\
        11. & p \equiv q \vdash r \supset p \supset (r \supset q)
            & MP(9, 10)
            \\
    \end{array}
    ```

    В шагах 6-8 можно было бы и не использовать теорему $`K`$, а воспользоваться следствием из теоремы дедукции, которое позволяет добавлять за знак штопора произвольные формулы: если $`\Gamma \vdash A`$, то $`\Gamma, \Delta \vdash A`$. Символы $`\Gamma`$ и $`\Delta`$ обозначают то же, что и раньше - некие множества формул.

    Второе утверждение (налево) доказывается аналогично.

## Домашнее задание

1. Завершить вывод из **задания 11**: доказать $`p \equiv q \vdash (r \supset q) \supset (r \supset p)`$.
2. Докажите теорему $`\lor_L`$, используя только те теоремы, которые используют операции импликации $`\supset`$ и отрицания $`\overline{p}`$.
3. Докажите теорему $`\land_L`$, используя только те теоремы, которые используют операции импликации $`\supset`$, отрицания $`\overline{\;}`$ и дизъюнкции $`\lor`$.
4. Используя вышеперечисленные результаты или любые другие теоремы с любыми операции, выполните задание 11 из *ДЗ2-1*. Напомню условие:
    ```math
    p \equiv q \vdash (p \lor r) \equiv (q \lor r)
    ```